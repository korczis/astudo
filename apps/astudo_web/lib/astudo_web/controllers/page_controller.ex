defmodule AstudoWeb.PageController do
  use AstudoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
