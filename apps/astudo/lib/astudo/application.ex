defmodule Astudo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: Astudo.PubSub}
      # Start a worker by calling: Astudo.Worker.start_link(arg)
      # {Astudo.Worker, arg}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Astudo.Supervisor)
  end
end
