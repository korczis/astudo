defmodule Astudo.Midi do
  alias Astudo.Native

  def list_inputs(), do: Native.midi_list_inputs()
  def list_outputs(), do: Native.midi_list_outputs()
end