defmodule NifNotLoadedError do
  defexception message: "nif not loaded"
end

defmodule AddStruct do
  defstruct lhs: 0, rhs: 0
end

defmodule Astudo.Native do
  @moduledoc """
  Astudo keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
  use Rustler,
      otp_app: :astudo,
      crate: :astudo

  defp err do
    throw(NifNotLoadedError)
  end

  def add(_a, _b), do: err()

  def midi_list_inputs(), do: err()
  def midi_list_outputs(), do: err()
  def midi_service_start(_), do: err()
end
