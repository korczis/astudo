// use std::env;
// use std::path::PathBuf;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    // tonic_build::configure()
    //     .file_descriptor_set_path(out_dir.join("midi.bin"))
    //     .compile(&[
    //         "proto/astudo_grpc/astudo_grpc.proto"
    //     ], &["proto"])
    //     .unwrap();

    tonic_build::compile_protos("proto/astudo_grpc/astudo_grpc.proto").unwrap();

    // tonic_build::configure()
    //     .build_server(false)
    //     .compile(
    //         &["proto/astudo_grpc/astudo_grpc.proto"],
    //         &["proto/astudo_grpc"],
    //     )?;

    Ok(())
}