#[macro_use]
extern crate lazy_static;
extern crate midir;

// use std::thread;
use std::pin;
use std::sync::Mutex;

use tokio::runtime::Runtime;

use rustler::thread;
use rustler::types::atom;
use rustler::{Atom, Encoder, Env};
use tonic::transport::Server;

pub mod midi;

use midi::engine::Engine;
use midi::input::Input;
use midi::output::Output;
use midi::service::start;

pub mod astudo_grpc {
    tonic::include_proto!("astudo_grpc");
}
use astudo_grpc::midi_service_server::{MidiService, MidiServiceServer};
use crate::midi::service::MidiServiceImplementation;

lazy_static! {
    pub static ref ENGINE: Mutex<Engine> = {
         Mutex::new(Engine::default())
    };

    // pub static ref HANDLE: Option<tokio::task::JoinHandle<Result<(), ()>>> = {
    //     None
    // };
    //
    pub static ref RUNTIME: Mutex<Runtime> = {
        Mutex::new(Runtime::new().unwrap())
    };

    // pub static ref HANDLE: tokio::task::JoinHandle<Result<(), ()>> = {
    //     tokio::spawn(async move {
    //         let _ = start().await.unwrap();
    //         Ok(())
    //     })
    // };
}

#[rustler::nif]
fn add(a: i64, b: i64) -> i64 {
    a + b
}

#[rustler::nif]
pub fn midi_list_inputs() -> Vec<Input> {
    ENGINE.lock().unwrap().inputs.clone()
}

#[rustler::nif]
pub fn midi_list_outputs() -> Vec<Output> {
    ENGINE.lock().unwrap().outputs.clone()
}

#[rustler::nif]
pub fn midi_service_start(env: Env, n: u64) -> Atom {
    // Multiply two numbers; panic on overflow. In Rust, the `*` operator wraps (rather than
    // panicking) in release builds. A test depends on this panicking, so we make sure it panics in
    // all builds. The test also checks the panic message.
    // fn mul(a: u64, b: u64) -> u64 {
    //     a.checked_mul(b).expect("threaded_fac: integer overflow")
    // }
    //
    // thread::spawn::<thread::ThreadSpawner, _>(env, move |thread_env| {
    //     // Create the runtime
    //     //let mut rt = Runtime::new().unwrap();
    //     let _ = RUNTIME.lock().unwrap().spawn(async {
    //         start().await.unwrap()
    //     });
    //
    //     let result: i32 = 0;
    //     result.encode(thread_env)
    // });

    atom::ok()
}

rustler::init!(
    "Elixir.Astudo.Native",
    [
        add,
        // midi
        midi_list_inputs,
        midi_list_outputs,
        midi_service_start
    ],
    load = load
);

fn load(_env: rustler::Env, _: rustler::Term) -> bool {
    ENGINE.lock().unwrap().init();

    let _ = RUNTIME.lock().unwrap().spawn(start());

    true
}