use std::error::Error;

pub mod midi;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    midi::service::start().await
}