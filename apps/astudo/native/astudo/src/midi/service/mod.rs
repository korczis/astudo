use tonic::{transport::Server, Request, Response, Status};

use futures::Stream;
use std::{error::Error, pin::Pin};
use tokio::sync::{mpsc, broadcast};
use tokio_stream::wrappers::ReceiverStream;

use astudo_grpc::midi_service_server::{MidiService, MidiServiceServer};
use astudo_grpc::{
    Input,
    ListInputsRequest,
    ListInputsResponse,
    ListOutputsRequest,
    ListOutputsResponse,
    MidiMessage,
    MidiStreamingRequest,
    // MidiStreamingResponse,
    Output
};

use midir::{MidiInput, Ignore};

use super::engine::Engine;

pub mod astudo_grpc {
    tonic::include_proto!("astudo_grpc");
}

type AstudoResult<T> = Result<Response<T>, Status>;
type MidiMessageStreamingResponseStream = Pin<Box<dyn Stream<Item = Result<MidiMessage, Status>> + Send>>;

pub struct MidiMessageHandler {
    pub tx: broadcast::Sender::<MidiMessage>,
    pub rx: broadcast::Receiver::<MidiMessage>,
    pub handle: tokio::task::JoinHandle<Result<(), ()>>
}

// #[derive(Default)]
// #[derive(Clone)]
pub struct MidiServiceImplementation {
    pub engine: Engine,

    pub tx: broadcast::Sender::<MidiMessage>,
    pub rx: broadcast::Receiver::<MidiMessage>,
    pub handle: tokio::task::JoinHandle<Result<(), ()>>,
}

impl MidiServiceImplementation {
    pub fn default() -> Self {
        println!("Initializing MyMidi");

        let (tx, rx) = broadcast::channel(16);
        let producer = tx.clone();

        let handle = tokio::spawn(async move {
            let mut midi_in = MidiInput::new("midir reading input").unwrap();
            midi_in.ignore(Ignore::None);
            let in_ports = midi_in.ports();

            let in_port = in_ports.get(0).ok_or("invalid input port selected").unwrap();
            let _conn_in = midi_in.connect(in_port, "midir-read-input", move |stamp, message, _| {
                println!("{}: {:?} (len = {})", stamp, message, message.len());
                let _ = producer.send(MidiMessage {
                    ts: stamp,
                    length: message.len() as u32,
                    data: message.iter().cloned().collect(),
                }).unwrap();
            }, ()).unwrap();

            loop {}
        });

        let mut engine = Engine::default();
        engine.init();

        MidiServiceImplementation {
            engine,

            handle,
            tx,
            rx
        }
    }
}

#[tonic::async_trait]
impl MidiService for MidiServiceImplementation {
    type MidiMessagesStreamStream = MidiMessageStreamingResponseStream;

    async fn list_inputs(
        &self,
        request: Request<ListInputsRequest>,
    ) -> Result<Response<ListInputsResponse>, Status> {
        println!("Got a request from {:?}", request.remote_addr());

        Ok(Response::new(astudo_grpc::ListInputsResponse {
            inputs: (&self.engine.inputs).into_iter().map(|item| Input{
                name: item.name.clone()
            }).collect()
        }))
    }

    async fn list_outputs(
        &self,
        request: Request<ListOutputsRequest>,
    ) -> Result<Response<ListOutputsResponse>, Status> {
        println!("Got a request from {:?}", request.remote_addr());

        Ok(Response::new(astudo_grpc::ListOutputsResponse {
            outputs: (&self.engine.outputs).into_iter().map(|item| Output{
                name: item.name.clone()
            }).collect()
        }))
    }

    async fn midi_messages_stream(
        &self,
        _req: Request<MidiStreamingRequest>,
    ) -> AstudoResult<MidiMessageStreamingResponseStream> {
        let (tx, rx) = mpsc::channel(128);
        let mut subscriber =  self.tx.subscribe();

        tokio::spawn(async move {
            while let Ok(msg) = subscriber.recv().await {
                println!("{:?}", &msg);

                match tx.send(Result::<_, Status>::Ok(msg)).await {
                    Ok(_) => {
                        // item (server response) was queued to be send to client
                    }
                    Err(_item) => {
                        // output_stream was build from rx and both are dropped
                        break;
                    }
                }
            };

            println!("\tclient disconnected");
        });

        let output_stream = ReceiverStream::new(rx);
        Ok(Response::new(
            Box::pin(output_stream) as Self::MidiMessagesStreamStream
        ))
    }
}

pub async fn start() -> Result<(), Box<dyn Error + Send + Sync>> {
    let addr = "127.0.0.1:50051".parse().unwrap();
    let midi_service = MidiServiceImplementation::default();

    Server::builder()
        .add_service(MidiServiceServer::new(midi_service))
        .serve(addr)
        .await?;

    Ok(())
}