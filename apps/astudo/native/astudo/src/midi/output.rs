use rustler::{NifStruct};

#[derive(Debug, Clone, NifStruct)]
#[must_use]
#[module = "Astudo.Midi.Output"]
pub struct Output {
    pub name: String
}