use rustler::{NifStruct};

#[derive(Debug, Clone, NifStruct)]
#[must_use]
#[module = "Astudo.Midi.Input"]
pub struct Input {
    pub name: String
}