use std::vec::Vec;

use crate::midi::input::Input;
use crate::midi::output::Output;

use midir::{MidiInput, MidiOutput, Ignore};

pub struct Engine {
    pub inputs: Vec<Input>,
    pub outputs: Vec<Output>
}

impl Engine {
    pub fn init(&mut self)  {
        println!("Initializing Astudo Midi Engine");
        self.refresh_ports();
    }

    pub fn default() -> Self {
        Engine {
            inputs: Vec::new(),
            outputs: Vec::new()
        }
    }

    fn refresh_ports(&mut self) {
        self.refresh_ports_input();
        self.refresh_ports_output();
    }

    fn refresh_ports_input(&mut self) {
        let mut midi_in = MidiInput::new("astudo::engine::Engine").unwrap();
        midi_in.ignore(Ignore::None);

        self.inputs = midi_in.ports().iter().enumerate().fold(Vec::new(), |mut acc, (_idx, el)| {
            acc.push(Input{
                name: midi_in.port_name(el).unwrap_or("Unknown".to_string())
            });

            acc
        })
    }

    fn refresh_ports_output(&mut self) {
        let midi_out = MidiOutput::new("astudo::engine::Engine").unwrap();

        self.outputs = midi_out.ports().iter().enumerate().fold(Vec::new(), |mut acc, (_idx, el)| {
            acc.push(Output{
                name: midi_out.port_name(el).unwrap_or("Unknown".to_string())
            });

            acc
        })
    }
}
