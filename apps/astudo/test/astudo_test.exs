defmodule AstudoTest do
  use ExUnit.Case, async: true
  doctest Astudo

  # , default_opts: &PropCheck.TestHelpers.config/0
  use PropCheck

  property "native add/2 works" do
    forall [a, b] <- [integer(), integer()] do
      a + b == Astudo.add(a, b)
    end
  end
 end
