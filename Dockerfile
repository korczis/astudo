# The version of Alpine to use for the final image
# This should match the version of Alpine that the `elixir:1.7.2-alpine` image uses
ARG ALPINE_VERSION=3.14.2

FROM elixir:1.13.4-alpine AS builder

# The following are build arguments used to change variable parts of the image.
# The name of your application/release (required)
ARG APP_NAME=astudo
ARG APP_VSN=0.1.0
ARG MIX_ENV=prod
ARG SKIP_PHOENIX=false
ARG TZ="Europe/Prague"
ARG HOST="localhost"

# If you are using an umbrella project, you can change this
# argument to the directory the Phoenix app is in so that the assets
# can be built
ARG PHOENIX_SUBDIR=.

ENV SKIP_PHOENIX=${SKIP_PHOENIX} \
    APP_NAME=${APP_NAME} \
    APP_VSN=${APP_VSN} \
    MIX_ENV=${MIX_ENV} \
    HOST=${HOST} \
    TZ=${TZ}

# By convention, /opt is typically used for applications
WORKDIR /opt/app

# This step installs all the build tools we'll need
RUN apk update && \
  apk upgrade --no-cache && \
  apk add --no-cache \
    autoconf \
    automake \
    bash \
    build-base \
    cairo-dev \
    cargo \
    erlang-dev \
    g++ \
    giflib-dev \
    git \
    graphviz \
    gzip \
    jpeg-dev \
    libpng \
    libpng-dev \
    libsass \
    libtool \
    make \
    nodejs \
    npm \
    openssl-dev \
    pango-dev \
    pkgconfig \
    pixman \
    pixman-dev \
    python3 \
    rust \
    sassc \
    tar \
    tzdata \
    --virtual build-dependencies && \
  mix local.rebar --force && \
  mix local.hex --force && \
  cp /usr/share/zoneinfo/Europe/Prague /etc/localtime && \
  echo "Europe/Prague" > /etc/timezone

RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.34-r0/glibc-2.34-r0.apk
RUN apk add glibc-2.34-r0.apk

RUN mkdir -p /opt/app/apps/astudo
RUN mkdir -p /opt/app/apps/astudo_web

COPY mix.exs mix.lock         /opt/app/
COPY apps/astudo/mix.exs     /opt/app/apps/astudo/
COPY apps/astudo_web/mix.exs /opt/app/apps/astudo_web/

RUN mix do deps.get, deps.compile

# RUN mix sass.install

## This copies our app source code into the build container
COPY . .

RUN mix cmd --app astudo_web mix phx.gen.cert

RUN mix do compile

RUN cd apps/astudo_web/assets && npm install

# This step builds assets for the Phoenix app (if there is one)
# If you aren't building a Phoenix app, pass `--build-arg SKIP_PHOENIX=true`
# This is mostly here for demonstration purposes
RUN if [ ! "$SKIP_PHOENIX" = "true" ]; then \
  mix cmd --app astudo_web mix esbuild default; \
  mix cmd --app astudo_web mix phx.digest; \
fi

RUN \
  mkdir -p /opt/built && \
  mix release

#RUN \
#  mkdir -p /opt/built && \
#  mix release && \
#  ls _build/${MIX_ENV}/rel/${APP_NAME}/releases/${APP_VSN}/ && \
#  cp _build/${MIX_ENV}/rel/${APP_NAME}/releases/${APP_VSN}/${APP_NAME}.tar.gz /opt/built && \
#  cd /opt/built && \
#  tar -xzf ${APP_NAME}.tar.gz && \
#  rm ${APP_NAME}.tar.gz

# From this line onwards, we're in a new image, which will be the image used in production
FROM elixir:1.13.4-alpine

ARG TZ="Europe/Prague"
ENV TZ=${TZ}

RUN apk update && \
  apk upgrade --no-cache && \
  apk add --no-cache \
    bash \
    git \
    graphviz \
    libpng \
    pixman \
    rust \
    sudo \
    tzdata \
    --virtual build-dependencies

RUN cp /usr/share/zoneinfo/Europe/Prague /etc/localtime && \
    echo "Europe/Prague" > /etc/timezone

ARG APP_NAME=astudo
ARG APP_VSN=0.1.0
ARG MIX_ENV=prod
ARG SECRET_KEY_BASE="xE/Owe8I2mABiP8XmdtXjW4hVEhRy8cZIe8MK10dEEnnFuV6mHRj0f337gaNIGXW"
ARG HOST="localhost"

ENV REPLACE_OS_VARS=true \
    APP_NAME=${APP_NAME} \
    APP_VSN=${APP_VSN} \
    MIX_ENV=${MIX_ENV} \
    SECRET_KEY_BASE=${SECRET_KEY_BASE} \
    HOST=${HOST}

WORKDIR /opt/app

COPY --from=builder /opt/app .

EXPOSE 4000
EXPOSE 4001

ENTRYPOINT [".docker/entrypoint.sh"]