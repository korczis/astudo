import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :astudo_web, AstudoWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "+CGM9gOWiZTwfn/liVcjl9CVcNn/FQWmo5KZK6gd49VMKdlaTS2iU3EFNLiyHd61",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# In test we don't send emails.
config :astudo, Astudo.Mailer, adapter: Swoosh.Adapters.Test

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
